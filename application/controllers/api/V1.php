<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// reference the Dompdf namespace
use Dompdf\Dompdf;
use chriskacerguis\RestServer\RestController;

class V1 extends RestController {

	protected $user_types=array();
    function __construct()
    {
        parent::__construct();
		$this->load->model('Api_model');
		$this->user_types = ['1'=>'Admin', '2'=>'Doctor', '3'=>'Lab Technician', '4'=>'Receptionist', '5'=>'Phlebotomist'];
    }

	
	public function deviceLogin_post($subdomain){
			

			//$subdomain ='dineshpanch';
			//$subdomain = $this->input->get('subdomain');
			$lab_subdomain_db = DB_PREFIX.$subdomain.'_db';
			$usernameORemail = $this->input->post('username');
			$password = $this->input->post('password');
			
				$conditions['where'] = array(
					'ls.employee_password'=>md5($password),
					'ls.status' =>'ACTIVE',
					'ls.is_deleted'=>'NO');

				$conditions['where_raw'][] = "( employee_email = '".$usernameORemail."' OR employee_username = '".$usernameORemail."' )";
				$conditions['where_raw'] = implode(" AND ",$conditions['where_raw']);
				
				$conditions['join'] = [[$lab_subdomain_db.'.labm_role lr', 'ls.role_id = lr.role_id', 'left']];
				$result = $this->Api_model->getRow($lab_subdomain_db.'.labm_staff ls', 'ls.*,lr.role_name,lr.role_access_level,lr.role_widget,lr.max_discount_allowed', $conditions);
			 	if(!empty($result))
				{
				
				$lab_config = $this->Api_model->getRow($lab_subdomain_db.'.labm_config','*',['where'=>['subdomain'=>$subdomain,'status'=>'ACTIVE','is_deleted'=>'NO']]);
				
				$lab_config_arr['labId'] =	 $lab_config->lab_id;
				$lab_config_arr['lab_name'] =	  $lab_config->lab_name;
				$lab_config_arr['lab_address'] = $lab_config->lab_address;
				$lab_config_arr['isActive'] =	  1;
				$lab_config_arr['labContact'] =	  $lab_config->lab_phone;
				$lab_config_arr['labAdminEmail'] =	$lab_config->lab_email;
				$lab_config_arr['lab_logo'] =	  $lab_config->lab_logo;
				$lab_config_arr['labName'] =	  $lab_config->lab_name;
				$lab_config_arr['labAbbreviation'] = $lab_config->lab_name;
				$lab_config_arr['stage'] = 'Sales Request';
				$lab_config_arr['subStage'] = '-';
				$lab_config_arr['accountsMgrId']['Id'] = '01';
				$lab_config_arr['accountsMgrId']['fullName'] = $lab_config->lab_name;
				$lab_config_arr['salesPersonId']['Id'] = '01';
				$lab_config_arr['salesPersonId']['fullName'] = $lab_config->lab_name;
				$lab_config_arr['otherDetails'] = '';
				
				
				$data['labDetails'] = $lab_config_arr;
				
				$condition['join'] = [[$lab_subdomain_db.'.labt_machine_device_config dc',"md.deviceId = dc.deviceId",'inner']];
				$condition['where'] = ['dc.labId'=>$lab_config->lab_id];
				$condition['order_by'] = ['md.id','asc'];
			
				$devicesDatanew = $this->Api_model->getRows($lab_subdomain_db.'.labm_machine_device md',"dc.deviceId,md.deviceName,md.deviceAuth,md.deviceType,md.localParser,dc.status,md.deviceIcon,dc.settings,dc.isBidirectional,dc.is_rate_limit_enabled,dc.rate_limit,dc.duration,dc.labId",$condition);
				//$devicesDatanew1 = $this->Api_model->getRows($lab_subdomain_db.'.labm_machine_device md', '*', ['join'=>[[$lab_subdomain_db.'.labt_machine_device_config dc',"md.deviceId = dc.deviceId",'inner']],'where'=>['dc.labId'=>$lab_config->lab_id]]);
			
				
		   	 $devicedata = array();
			
			
			foreach ($devicesDatanew as $row) {
				$devicedata[] = $row;
				}
				$data['devices'] = $devicedata; 
				$this->response( [
					'status' => false,
					'0' => $data,
					'code' => 200,
				], 200 );	
		
				}else{
					$this->response( [
							'code' => 500,
							'message' => 'Username or password is incorrect',
							
						], 500 );
				}
	}
	
	
	
	public function dataPartialFromDevice_post($subdomain)
    { 
		//$subdomain ='dineshpanch';
		//$subdomain = $this->input->get('subdomain');
		$data = $this->input->post('data');
		$data = json_decode($data,true);
		
		$lab_subdomain_db = DB_PREFIX.$subdomain.'_db';
		$deviceId = $data['deviceId'];
		$deviceAuth = $data['deviceAuth'];
		$dateTime = $data['dateTime'];
		$sampleId = $data['sampleId'];
		$isSampleMatched = $data['isSampleMatched'];
		$testJsonData = $data['data'];


		if(!empty($testJsonData)){
			foreach($testJsonData['values'] as $val){
				$rowInfo = array(
					'deviceId'=>$deviceId,
					'dateTime'=>$dateTime,
					'deviceAuth'=>$deviceAuth,
					'sampleId'=>$sampleId,
					'para_code'=>$val['testName'],
					'value'=>$val['value'],
					'isSampleMatched'=>$isSampleMatched,
					//'testJsonData'=> json_encode($testJsonData)
				);
				
				$checkparaval = $this->Api_model->getRow($lab_subdomain_db.'.labt_machine_device_test_data','id',['where'=>['deviceId'=>$deviceId,'sampleId'=>$sampleId,'para_code'=>$val['testName']]]);

				if(!empty($checkparaval)){
					$result = $this->Api_model->edit($lab_subdomain_db.'.labt_machine_device_test_data', $rowInfo, ['id'=>$checkparaval->id]);
				}
				else{
					$result = $this->Api_model->add($lab_subdomain_db.'.labt_machine_device_test_data', $rowInfo);
				}
				
			}
		}
		if($result > 0)
		{
			
			$this->response( [
					//'0' => $data,
					'code' => 200,
				], 200 );
				
		}
		
	}
	
	public function deviceWaitingList_post($subdomain)
    { 
		
		//$subdomain ='dineshpanch';
		//$subdomain = $this->input->get('subdomain');
		$lab_subdomain_db = DB_PREFIX.$subdomain.'_db';
		//$labId = $this->input->post('labId');
		$deviceId = $this->input->post('deviceId');
		
		
		$conditions['join'] = [[$lab_subdomain_db.'.labt_machine_device_test mt',' tv.test_id = mt.test_id ','inner'],
		[$lab_subdomain_db.'.labm_test lt','tv.test_id = lt.test_id ','left'],
		[$lab_subdomain_db.'.labt_appointment ap ','tv.appointment_id = ap.appointment_id','left'],
		[$lab_subdomain_db.'.labm_patient pt','ap.patient_id=pt.patient_id','left']];
		$conditions['where'] = ['ap.status'=>'OPENED','ap.is_deleted'=>'NO','mt.deviceId'=>$deviceId];
		
		$conditions['where_raw'] = "(tv.test_process_status = 'SAMPLE COLLECTED' OR tv.test_process_status = 'WAITING')";
		//$conditions['where_raw'][] = "(mt.deviceId = $deviceId )";
			
		$records = $this->Api_model->getRows($lab_subdomain_db.'.labt_patient_test_values tv', "tv.sample_id,pt.patient_name,lt.test_name,",$conditions);
		$this->response( [
					'responseText' => $records,
					'status' => 200,
					'readyState'=>4,
					], 200 );
		
	}
	
}
